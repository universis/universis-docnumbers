/// <reference path="./DocumentNumberSeriesItem.d.ts" />
import {DataObject, EdmMapping, EdmType, PermissionMask, DataPermissionEventListener} from '@themost/data';
import {readStream} from "@themost/express";
import { Guid, HttpError } from '@themost/common';
import {promisify} from 'util';
import { DataNotFoundError, HttpForbiddenError } from "@themost/common";

@EdmMapping.entityType('DocumentNumberSeriesItem')
class DocumentNumberSeriesItem extends DataObject {
    //
    async newCode() {
        return Guid.newGuid().toString().replace(/-/g,'');
    }

    /**
     * @param {Buffer|ArrayBuffer|ReadableStream} file
     * @param {*} attributes
     * @returns Promise<DocumentNumberSeriesItem>
     */
    @EdmMapping.param('attributes', 'Object', true, true)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('replace','DocumentNumberSeriesItem')
    async replace(file, attributes) {
        let blob;
        // get file buffer
        if (file instanceof Buffer) {
            blob = file;
        } else if (file instanceof ArrayBuffer) {
            blob = new Uint8Array(file);
        } else {
            blob = await readStream(file);
        }
        // get extra attributes and assign id
        const extraAttributes = Object.assign({
            contentType: file.contentType,
            name: file.contentFileName
        }, attributes, {
            id: this.getId()
        });
        // get document service by name
        const documentService = this.context.getApplication().getService(function DocumentNumberService(){
        });
        // throw exception if document service is null
        if (documentService == null) {
            throw new Error('Document service cannot be found or is inaccessible.');
        }
        // replace file
        await documentService.replaceFrom(this.context, blob, extraAttributes);
        // return extraAttributes object
        return extraAttributes;
    }

    /**
     * Reverts the publication of a DocumentNumberSeriesItem. Useful for mass cancel publication actions, 
     * where a specific privilege has to be set in order for the action to be completed.
     */
     @EdmMapping.action('cancelPublication', 'DocumentNumberSeriesItem')
     async cancelPublication() {
         // get validator listener
         const validator = new DataPermissionEventListener();
         // noinspection JSUnresolvedFunction
         const validateAsync = promisify(validator.validate).bind(validator);
         // validate DocumentNumberSeriesItem/CancelPublication execute permission
         const event = {
             model: this.getModel(),
             privilege: 'DocumentNumberSeriesItem/CancelPublication',
             mask: PermissionMask.Execute,
             target: this,
             throwError: false
         }
         await validateAsync(event);
         if (event.result === false) {
             throw new HttpForbiddenError();
         }
         const model = this.context.model('DocumentNumberSeriesItem');
         // get item
         const item = await model.where('id').equal(this.getId()).getItem();
         if (item == null) {
             throw new DataNotFoundError('The item is inaccessible');
         }
         // cancel item publication
         if (item && typeof item.published !== 'undefined') {
             if (item.published === false) {
                 // do nothing
                 return item;
             } else {
                 item.published = false;
                 item.datePublished = null;
             }
            // save item
            return await model.save(item);
         }
     }

     /*
        Verifies the document's signature(s), via SignerWorker service
     */
    @EdmMapping.func('VerifyDocumentSignature')
    async verifyDocumentSignature() {
        const context = this.context, application = context.getApplication();
        // get self
        const self = await context.model('DocumentNumberSeriesItem')
            .where('id').equal(this.getId())
            .getItem();
        if (self == null) {
            throw new DataNotFoundError('The specified document cannot be found or is inaccessible');
        }
        // try to get SignerWorker service
        const signerWorker = application.getService(function SignerWorker() {});
        if (signerWorker == null) {
            // if it is not present, throw error
            throw new HttpError(500, context.translate('UnableToVerifyDocumentSignature'));
        }
        // get private content service
        const privateContentService = application.getService(function PrivateContentService() {});
        if (privateContentService == null) {
            // if it is not present, throw error
            throw new HttpError(500, context.translate('UnableToVerifyDocumentSignature'));
        }
        const attachment = self;
        // get the attachment's physical path
        const resolvePhysicalPath = promisify(privateContentService.resolvePhysicalPath).bind(privateContentService);
        const physicalPath = await resolvePhysicalPath(context, attachment);
        if (physicalPath == null) {
            throw new HttpError(500, context.translate('UnableToVerifyDocumentSignature'));
        }
        const signatureVerificationResults = await signerWorker.verify(context, physicalPath, {
            contentType: attachment.contentType,
            filename: attachment.name
        });
        // return the verification results
        return {
            signatureVerificationResults,
            valid: Array.isArray(signatureVerificationResults) &&
                signatureVerificationResults.length > 0 &&
                    signatureVerificationResults.every((signature) => signature.valid)
        }
    }

    @EdmMapping.func('CanSignDocument', EdmType.EdmBoolean)
    async canSignDocument() {
        // get and validate self
        const context = this.context;
        const self = await context.model(this.getModel().name)
            .where('id').equal(this.getId())
            .select('id', 'isBasedOn', 'signed', 'documentStatus/alternateName as documentStatus')
            .expand({
                name: 'isBasedOn',
                options: {
                    $expand: 'signRoles'
                }
            })
            .getItem();
        if (self == null) {
            throw new DataNotFoundError('The specified document cannot be found or is inaccessible');
        }
        // check if document is cancelled or signed
        if (self.documentStatus === 'CancelledDocumentStatus' || self.signed === true) {
            return false;
        }
        const signRoles = self.isBasedOn && self.isBasedOn.signRoles;
        // check if the related report template has sign roles defined
        if (signRoles == null || (Array.isArray(signRoles) && signRoles.length === 0)) {
            return true;
        }
        // get active sign action of current user
        const owner = context.interactiveUser || context.user;
        const activeSignAction = await context.model('DocumentSignAction')
            .where('documentNumberSeriesItem').equal(self.id)
            .and('actionStatus/alternateName').equal('ActiveActionStatus')
            .and('owner/name').equal(owner && owner.name)
            .select('id')
            .silent()
            .count();
        return !!activeSignAction;
    }

}

module.exports = DocumentNumberSeriesItem;
