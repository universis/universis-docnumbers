/// <reference path="./DocumentNumberSeries.d.ts" />
import {Args, DataError} from '@themost/common';
import {DataObject, EdmMapping} from '@themost/data';
import numeral from 'numeral';
import moment from 'moment';

@EdmMapping.entityType('DocumentNumberSeries')
class DocumentNumberSeries extends DataObject {
    /**
     * @param {*=} extraAttributes 
     * @returns 
     */
    async next(extraAttributes) {
        /**
         * get document service
         * @type {DocumentNumberService}
         */
        const service = this.context.getApplication().getService(function DocumentNumberService() {});
        Args.check(service != null, 'DocumentNumberService may not be null');
        return await service.next(this.context, this, extraAttributes);
    }
    /**
     * 
     * @param {*} index 
     * @param {DocumentNumberFormatOptions=} formatOptions 
     * @return {string}
     */
    async format(index, formatOptions) {
        // get format params
        const options = Object.assign({ 
            currentDate: new Date()
         }, formatOptions);
         // validate parent numberFormat
         if (Object.prototype.hasOwnProperty.call(this, 'useParentIndex') === false) {
             this.useParentIndex = await this.context.model(this.getType())
                .where('id').equal(this.getId())
                .select('useParentIndex').silent().value();
         }
         if (this.useParentIndex) {
            // get parent
            this.parent = await this.property('parent').silent().getItem();
            if (this.parent == null) {
                throw new DataError('E_FOUND', 'Parent document number series item cannot be found or is inaccessible', null, 'DocumentNumberSeries', 'parent');
            }
            this.numberFormat = this.parent.numberFormat;
         }
         // fetch numberFormat if empty
        if (this.numberFormat == null) {
            this.numberFormat = await this.context.model(this.getType())
                .where('id').equal(this.getId())
                .select('useParentIndex').silent().value();
        }
        Args.notNull(this.numberFormat, 'Document number format');
        let result = this.numberFormat;
        // first find number parameter e.g. {0000} or {0}
        let match = /\{(\d+)\}/.exec(result);
        if (match) {
            // e.g. match[0] => {0000} and match[1] => 0
            const formattedIndex = numeral(index).format(match[1]);
            // replace formatted index
            result = result.replace(/\{(\d+)\}/, formattedIndex);
        }
        else {
            // throw error for invalid expression
            throw new Error('Invalid document number format expression. Index parameter is missing');
        }
        if (formatOptions.academicYear) {
            const academicYear = this.context.model('AcademicYear').convert(formatOptions.academicYear).getId();
            if (Number.isInteger(academicYear) === false) {
                throw new DataError('E_VALUE', 'Invalid academic year. Expected number.', null, 'AcademicYear', 'id');
            }
            if (/{AAAAAA}/g.test(result)) {
                // replace {AAAAAA} with academicYear long format e.g 2019-2020
                result = result.replace(/{AAAAA}/i, `${academicYear}-${academicYear+1}`);
            }
            if (/{AAAAA}/g.test(result)) {
                // replace {AAAAA} with academicYear alternate name e.g 2019-20
                result = result.replace(/{AAAAA}/i, `${academicYear}-${(academicYear+1).toString().substr(2)}`);
            }
            if (/{AAAA}/g.test(result)) {
                // replace {AAAA} with academic year e.g 2019
                result = result.replace(/{AAAA}/i, academicYear);
            }
        }

        // format date parameters if any
        const re = /\{(.*?)\}/i;
        match = re.exec(result);
        let momentInstance = moment(options.currentDate);
        while(match) {
            // e.g. match[0] => {YYYY} and match[1] => YYYY
            let replaceMatch = momentInstance.format(match[1]);
            result = result.replace(match[0], replaceMatch);
            match = re.exec(result);
        }
        return result;
    }
    /**
     * Gets next index and format document number
     * @param {DocumentNumberFormatOptions=} formatOptions 
     * @return {string}
     */
    async nextAndFormat(formatOptions) {
        const nextIndex = await this.next();
        return this.format(nextIndex, formatOptions);
    }
}
module.exports = DocumentNumberSeries;