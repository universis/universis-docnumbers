import DocumentNumberSeries from './models/DocumentNumberSeries';
import DocumentNumberSeriesItem from './models/DocumentNumberSeriesItem';
import DepartmentDocumentNumberSeriesItem from './models/DepartmentDocumentNumberSeries';
export * from './DocumentNumberService';
export * from './DefaultDocumentNumberService';
export {DocumentNumberSeries,
    DocumentNumberSeriesItem,
    DepartmentDocumentNumberSeriesItem};
export * from './DocumentNumberVerifierService';
export declare const MEDIA_TYPES: Map<string, string>;
