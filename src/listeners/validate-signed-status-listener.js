import { Args, LangUtils } from '@themost/common';
import { DataObjectState } from '@themost/data';
const parseBoolean = LangUtils.parseBoolean;
/**
 * @param {import('@themost/data').DataEventArgs} event
 */
async function beforeSaveAsync(event) {
  // operate only on update
  if (event.state !== DataObjectState.Update) {
    return;
  }
  const context = event.model.context,
    target = event.target,
    previous = event.previous;
  Args.check(
    previous != null,
    'The previous state of the object cannot be determined'
  );
  // assign internal signed property (to handle document reissue)
  target._signed = parseBoolean(target.signed);
  // provide all sign actions to the following listeners (to avoid multiple queries)
  const signActions = await context
    .model('DocumentSignAction')
    .where('documentNumberSeriesItem')
    .equal(previous.id)
    .silent()
    .getAllItems();
  Object.assign(target, {
    signActions
  });
  // continue only if the target has the signed property
  if (Object.prototype.hasOwnProperty.call(target, 'signed') === false) {
    return;
  }
  // when the document is flaged as signed
  if (
    parseBoolean(previous.signed) === false &&
    parseBoolean(target.signed) === true
  ) {
    // if it is not related to an sign action
    if (!(Array.isArray(signActions) && signActions.length > 0)) {
      // exit
      return;
    }
    // check if every role has signed the document
    const allRolesSigned = signActions.every((action) => {
      const role = action.departmentRole;
      return signActions.some((signAction) => {
        return (
          signAction.departmentRole === role &&
          signAction.actionStatus && signAction.actionStatus.alternateName === 'CompletedActionStatus'
        );
      });
    });
    // if they have
    if (allRolesSigned) {
      // exit
      return;
    }
    // else, the document cannot be considered as fully signed
    // note: an error could be thrown here, but this simplifies the process
    target.signed = false;
  }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}
