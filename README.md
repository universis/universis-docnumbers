# @universis/docnumbers

Universis api server default document numbering service

## Installation

npm i @universis/docnumbers

## Usage

Register `DocumentNumberService` in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/docnumbers#DocumentNumberService",
            "strategyType": "@universis/docnumbers#DefaultDocumentNumberService"
        }
    ]

Add `DocumentNumberSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/docnumbers#DocumentNumberSchemaLoader"
                    }
                ]
            }
        }
    }

## Configuration

Use `settings/universis/docnumbers` section to configure the usage of document verifier services:

     "docnumbers": {
        "origin": "https://api.example.com/",
        "rateLimit": {
            "windowMs": 300000
            "max": 5
        }
      }



### origin

> string

A string which represents the remote address where document verifier service will be accessible by end-users.

### rateLimit

> object

`@universis/docnumbers` uses [express-rate-limit](https://github.com/nfriedly/express-rate-limit#configuration) configuration for limiting the amount of requests against verification services. The default values are:

    {
        windowMs: 300000, // 5 minutes
        max: 20, // 20 requests
        standardHeaders: true,
        legacyHeaders: false
    }

#### shortDocumentCodeDuration

> string

An [ISO 8601 duration](https://en.wikipedia.org/wiki/ISO_8601#Durations) which represents the amount of time where the usage of short document code will be valid. The default value is `P1M`

    {
        "shortDocumentCodeDuration": "P2M" // two months
    }
